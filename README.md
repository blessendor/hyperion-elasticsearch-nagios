# hyperion-elasticsearch-nagios

Simple icinga2/nagios plugin script to check Elasticsearch health status of Hyperion (https://hyperion.docs.eosrio.io/)

It analyze health status from the json url and compare "last_indexed_block" vs "total_indexed_blocks" (must be equal for OK state).

