#!/usr/bin/env python3
# Oleksandr Usov (blessendor@gmail.com)
# 2021-03-01 first production version deployed
import sys
import urllib.request
import json
import ssl
import ast

# sys.argv[0] argument is a script itself (returns full path to file hyperion_elasticsearch.py)

# Get url from first argument or exit
if len(sys.argv)<2:
    print ("URL is not defined - required to check Hyperion (Elasticsearch) status! \n")
    print ("Usage:", sys.argv[0], "'HTTP(S)://URL'")
    exit(3) # exit code '3' - returns Unknown for nagios service state

# Set URL var from the script argument
data_url = sys.argv[1]

# Re-define URL for dev/debug mode without arguments:
#data_url="https://wax.eu.eosamsterdam.net/v2/health"

# Check if provide URL/path is OK to open - exit with CRITICAL state and error msg

try:
    urllib.request.urlopen(data_url)
except urllib.error.HTTPError as h_err:
        # HTTPError exceptoion - when error in /../path
        err_msg = "CRITICAL - can't open URL ../path/:\n\n{}\n\nURL: {}"
        print(err_msg.format(h_err,data_url))
        exit(2) # exit code '2' - returns Critical for nagios service state
except urllib.error.URLError as u_err:
        # URLError exceptoion - when problem with hostname resolving/opening
        err_msg = "CRITICAL - can't resolve URL hostname:\n\n{}\n\nURL: {}"
        print(err_msg.format(u_err.reason,data_url))
        exit(2) # exit code '2' - returns Critical for nagios service state
else:
    with urllib.request.urlopen(data_url) as url:
        data = json.loads(url.read().decode())
        find_health = data.items()
        for i in find_health:
            if 'health' in i:
                # convert to <class 'str'>
                health_raw=json.dumps(i) 
                # remove useless chars
                health_raw_replace_left = health_raw.replace('["health", [', '')
                health_raw_replace_final = health_raw_replace_left.replace(']]', '')
                # define tuple of 3 items (Elasticsearch is interested)
                services = ast.literal_eval(health_raw_replace_final)
                # convert tuple item to dict
                RabbitMq = (services[0])
                NodeosRPC = (services[1])
                Elasticsearch = (services[2])

                #Only Elasticsearch will be tested by this script
                elasticsearch_status_data = Elasticsearch.get('service_data')

                if  elasticsearch_status_data.get('last_indexed_block') == elasticsearch_status_data.get('total_indexed_blocks'):

                    status_msg = "OK - last_indexed_block ({}) = ({}) total_indexed_blocks\nURL: {}"
                    print(status_msg.format(elasticsearch_status_data.get('last_indexed_block'),\
                                            elasticsearch_status_data.get('total_indexed_blocks'), data_url))
                    exit(0)  # exit code '0' - returns OK for nagios service state

                else:
                    status_msg = "CRITICAL - last_indexed_block ({}) != ({}) total_indexed_blocks\nURL: {}"
                    print(status_msg.format(elasticsearch_status_data.get('last_indexed_block'),\
                                            elasticsearch_status_data.get('total_indexed_blocks'), data_url))
                    exit(2)  # exit code '2' - returns Critical for nagios service state


